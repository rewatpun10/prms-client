import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { tap,map  } from 'rxjs/operators';
import { Router } from '@angular/router';


interface UsernameAvailabilityResponse {
available: boolean;
}

interface UserCredentialsResponse {
jwtToken:string;
}

interface UserCredentials {
username: string;
password: string;
}

interface SignUpResponse {
username: string;
}

interface SignedInResponse {
authenticated: boolean;
username: string;
}

  var headers_object = new HttpHeaders()
               .set("Authorization", 'Bearer ' + sessionStorage.getItem('token'));
               const httpOptions = {
                 headers: headers_object
               };

@Injectable({
  providedIn: 'root'
})

export class AuthService {
username:string;
password: string;

//   rootUrl = 'https://api.angular-email.com';
  rootUrl = 'http://localhost:8080';
  signedIn$ = new BehaviorSubject(null);

  constructor(private http: HttpClient, private router: Router) {
   }


      //checks username availability
      checkUsernameAvailability(username:string) {
      return this.http.post<UsernameAvailabilityResponse>(this.rootUrl + '/auth/username', {
      username
      });
      }

      //signup process
      signup(userCredentials: UserCredentials) {
       return this.http.post<SignUpResponse>(
       this.rootUrl+'/auth/signup',
       userCredentials)
       .pipe(
          tap(() => {
          this.signedIn$.next(true);
          })
       );
       }

      //checking if user is signed in
       checkAuthentication() {
       if(sessionStorage.getItem('token') != null) {
        var headers_object = new HttpHeaders()
                      .set("Authorization", 'Bearer ' + sessionStorage.getItem('token'));
                      const httpOptions = {
                        headers: headers_object
                      };
       console.log('sdfsdf',httpOptions);
        return this.http.get<any>(this.rootUrl+ '/auth/signedIn' , httpOptions)
        .pipe(
          tap((response) => {
          console.log('ttt', response);



            this.signedIn$.next(response);
              this.router.navigateByUrl('/dashboard');
          })
           )
           } else {
            this.signedIn$.next(false);
             this.router.navigateByUrl('/');
            }

       }

       //sign0ut process
       signOut() {
       console.log('signOut');
//        return this.http.get<any>(this.rootUrl+'/logout', httpOptions )
//            .pipe(
//                   tap((response) => {
//                   this.signedIn$.next(false);
//                   console.log('response',response);
//                   })
//                   )
          sessionStorage.removeItem('token');
          localStorage.removeItem('username');
          localStorage.removeItem('userrole');
          this.checkAuthentication();
       }

       //singIn process
       signIn(userCredentials){
              console.log('signIn');

        return this.http.post<any>(this.rootUrl+'/api/authenticate', userCredentials)
        .pipe(
           tap((response) => {
                this.signedIn$.next(true);
                let tokenStr = response.jwttoken;
                let role = response.role;
                       console.log('sign', role);

                let username = response.username;
                       console.log('signdfgfg', username);

                sessionStorage.setItem("token", tokenStr);
                localStorage.setItem("userrole", role);
                                       console.log('signSDDFDF', localStorage.getItem('role'));

                localStorage.setItem("username", username );
           })

           )
       }
}
