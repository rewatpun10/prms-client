import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {MatchPassword} from '../Validators/match-password';
import { UniqueUsernameCheck} from '../validators/unique-username-check';
import {AuthService} from '../auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  authForm = new FormGroup({
  username: new FormControl('', [
  Validators.required,
  Validators.minLength(3),
  Validators.maxLength(20),
  Validators.pattern(/^[a-z0-9]+$/)
  ], [this.uniqueUsernameCheck.validate]),

  password: new FormControl('',[
   Validators.required,
   Validators.minLength(4),
   Validators.maxLength(20)
  ]),

  passwordConfirmation: new FormControl('',[
  Validators.required,
  Validators.minLength(4),
  Validators.maxLength(20)
  ])
  }, {validators:[this.matchPassword.validate]});

  constructor(
  private matchPassword: MatchPassword,
  private uniqueUsernameCheck: UniqueUsernameCheck,
  private authService: AuthService,
  private router: Router
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    if(this.authForm.invalid) {
    return;
    }
    this.authService.signup(this.authForm.value)
    .subscribe({
    next: (response) => {
        this.router.navigateByUrl('/dashboard');
    },

    error: (err) => {
    if(!err.status) {
    this.authForm.setErrors({noConnection: true})
    }else {
    this.authForm.setErrors({unKnownError: true})
    }
    }

    });
  }

}
