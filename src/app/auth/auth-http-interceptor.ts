import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpEventType } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap, filter } from 'rxjs/operators';

@Injectable()
export class AuthHttpInterceptor implements HttpInterceptor{

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
    ): Observable<HttpEvent<any>> {
    const modifiedrequest = request.clone({
    withCredentials: true
    });

    return next.handle(modifiedrequest);

//     .pipe(
//     filter(val => val.type === HttpEventType.Sent),
//     tap(val => {
//          console.log('Request was sent');
//        })
//     );

    }
}
