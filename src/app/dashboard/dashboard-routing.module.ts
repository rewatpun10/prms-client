import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardHomeComponent } from './dashboard-home/dashboard-home.component';
import { MedicationComponent } from './medication/medication.component';
import { PrescriptionComponent } from './prescription/prescription.component';
import { UsersComponent } from './users/users.component';
import { PrescribedMedicationsComponent } from './prescribed-medications/prescribed-medications.component';


const routes: Routes = [

{path: '' , component: DashboardHomeComponent,
  children: [
    {path: '', component: MedicationComponent},
    {path: 'medications', component: MedicationComponent},
    {path: 'prescription', component: PrescriptionComponent},
    {path: 'users', component: UsersComponent},
    {path: 'prescribedMedications', component: PrescribedMedicationsComponent}
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
