import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { DashboardService } from '../dashboard.service';

import { Router} from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  authForm = new FormGroup({
    username: new FormControl('', [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(20),
    ]),

    password: new FormControl('',[
     Validators.required,
     Validators.minLength(4),
     Validators.maxLength(20)
    ])

    });

  constructor(private dashboardService: DashboardService, private router: Router) { }
  patientList = [];
  ngOnInit() {
  this.dashboardService.getAllUsers()
  .subscribe((patients) => {
    this.patientList = patients;
    console.log('pa', this.patientList);
  });
  }

   onSubmit() {
        this.dashboardService.createUser(this.authForm.value)
        .subscribe({
              next: () => {
                let currentUrl = this.router.url;
                                 this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
                                        this.router.navigate([currentUrl]);
                                    });
              }
    });
    }

}
