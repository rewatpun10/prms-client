import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { DashboardService } from '../dashboard.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-prescribed-medications',
  templateUrl: './prescribed-medications.component.html',
  styleUrls: ['./prescribed-medications.component.css']
})
export class PrescribedMedicationsComponent implements OnInit {
checkedId:string;
prescribedMedicationsForm = new FormGroup({
  doesTaken: new FormControl()
})


  userMedications: any = [];
       prescribedMedicationTakenList = [];
  constructor(private dashboardService: DashboardService, private router: Router) { }

  ngOnInit() {
            this.dashboardService.getUserMedications()
            .subscribe(data => {
              this.userMedications = data;
             console.log('userMedications', this.userMedications);

            });


         this.dashboardService.getMedicationTakenByUser()
         .subscribe((response) => {
           this.prescribedMedicationTakenList = response;
           console.log('pml', this.prescribedMedicationTakenList);
         });

  }

  check(values, id){
    this.checkedId = id;
  }

   onSubmit() {
          this.dashboardService.createMedicationRecord(this.checkedId)
                   .subscribe({
                    next: () => {
                    let currentUrl = this.router.url;
                    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
                     this.router.navigate([currentUrl]);
                     });
                    }
                    });
          }

}
