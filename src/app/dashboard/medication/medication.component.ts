import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { DashboardService } from '../dashboard.service';

import { Router} from '@angular/router';



@Component({
  selector: 'app-medication',
  templateUrl: './medication.component.html',
  styleUrls: ['./medication.component.css']
})

export class MedicationComponent implements OnInit {

  authForm = new FormGroup({
   medicine_name: new FormControl('',[
     Validators.required,
     Validators.minLength(4),
     Validators.maxLength(20)
    ]),

  manufactured_by: new FormControl('',[
       Validators.required,
       Validators.minLength(4),
       Validators.maxLength(20)
   ]),

  dosage: new FormControl('',[
         Validators.required,
         Validators.minLength(4),
         Validators.maxLength(20)
        ])
  });
  constructor(private dashboardService: DashboardService, private router: Router) { }
  medicationsList = [];

  ngOnInit() {
  this.dashboardService.getAllMedications()
  .subscribe((medications) => {
    this.medicationsList = medications;
  });
  }

  onSubmit() {
      this.dashboardService.createMedications(this.authForm.value)
      .subscribe({
                next: () => {
                let currentUrl = this.router.url;
                 this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
                        this.router.navigate([currentUrl]);
                    });
                }
                });
      }
}
