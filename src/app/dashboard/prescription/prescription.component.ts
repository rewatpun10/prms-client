import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { DashboardService } from '../dashboard.service';
import { Router} from '@angular/router';


@Component({
  selector: 'app-prescription',
  templateUrl: './prescription.component.html',
  styleUrls: ['./prescription.component.css']
})
export class PrescriptionComponent implements OnInit {

prescriptionForm = new FormGroup({
  patient: new FormControl(),
  medication: new FormControl()
})
  constructor(private dashboardService: DashboardService, private router: Router) { }
  users: any = [];
  medications: any = [];
  ngOnInit() {
          this.dashboardService.getAllUsers()
            .subscribe(data => {
              this.users = data;
            });

            this.dashboardService.getAllMedications()
              .subscribe(data => {
                this.medications = data;
             });

  }

  onSubmit() {
        this.dashboardService.createPrescription(this.prescriptionForm.value)
        .subscribe({
                  next: () => {
                  this.router.navigateByUrl('/dashboard/medications');
                  }
                  });
        }

}
