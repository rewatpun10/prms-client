import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';


import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardHomeComponent } from './dashboard-home/dashboard-home.component';
import { MedicationComponent } from './medication/medication.component';
import { PrescriptionComponent } from './prescription/prescription.component';
import {SharedModule} from '../shared/shared.module';
import { UsersComponent } from './users/users.component';
import { PrescribedMedicationsComponent } from './prescribed-medications/prescribed-medications.component';



@NgModule({
  declarations: [DashboardHomeComponent, MedicationComponent, PrescriptionComponent, UsersComponent, PrescribedMedicationsComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ReactiveFormsModule,
    SharedModule
   ]
})
export class DashboardModule { }
