import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse ,HttpHeaders } from '@angular/common/http';
import { tap  } from 'rxjs/operators';



interface Medications {
id: number,
dosage: string,
manufacturedBy: string,
medicineName: string
}

var headers_object = new HttpHeaders()
        .set("Authorization", 'Bearer ' + sessionStorage.getItem('token'));

        const httpOptions = {
          headers: headers_object
        };

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  rootUrl = 'http://localhost:8080';
  constructor(private http: HttpClient ) {

  }

  getMedications() {
//   return this.http.get<Medications[]>(this.rootUrl+'/');
return null;
  }

  createMedications(medicationDetails) {
   return this.http.post<Medications>(this.rootUrl+'/api/admin/medication', medicationDetails, httpOptions)
    .pipe(
           tap((response) => {
              console.log('response',response);
               })
               )
  }

  createUser(userDetails) {
  console.log('userDetails', userDetails);
    return this.http.post<any>(this.rootUrl+'/api/admin/create-patient', userDetails, httpOptions)
      .pipe(
             tap((response) => {
                console.log('response',response);
                 })
             )
    }


  createPrescription(prescriptionDetails) {
  const medication_id = [];
  let user = prescriptionDetails.patient;
  medication_id.push(prescriptionDetails.medication);
  console.log('pd', user, medication_id);
   return this.http.post<Medications>(this.rootUrl+'/api/admin/prescription', {user, medication_id}, httpOptions)
    .pipe(
           tap((response) => {
              console.log('response',response);
               })
               )
  }

  getAllUsers() {
      return this.http.get<any>(this.rootUrl+'/api/admin/patient', httpOptions)
            .pipe(
                   tap((response) => {
                      console.log('response',response);
                       })
                   )
  }

    getAllMedications() {
        return this.http.get<any>(this.rootUrl+'/api/admin/medications', httpOptions)
              .pipe(
                     tap((response) => {
                      })
                     )
    }


    //get Users medications

      getUserMedications() {
          return this.http.get<any>(this.rootUrl+'/api/patient/medication', httpOptions)
                .pipe(
                       tap((response) => {
                          console.log('response',response);
                           })
                       )
      }

      createMedicationRecord (values) {
      console.log('httpOptions' , httpOptions);
       return this.http.post<any>(this.rootUrl+'/api/patient/usermed', {id: values}, httpOptions)
            .pipe(
                   tap((response) => {
                      console.log('response',response);
                       })
                   )
      }

      getMedicationTakenByUser() {
          return this.http.get<any>(this.rootUrl+'/api/patient/getMedicationTaken', httpOptions)
            .pipe(
                   tap((response) => {
                      console.log('response',response);
                       })
                   )
      }
}
